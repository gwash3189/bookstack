var mongo = require('mongodb');
exports.Database = function (address, port, dbName, dbCollectName) {
    var mongoServer = mongo.Server;
    var Db = mongo.Db;
    this.dbName = dbName;
    this.dbCollectionName = dbCollectName;
    this.server = new mongoServer();
    this.db = new Db(dbName, this.server);
    this.BSON = mongo.BSONPure;
}

/**
 * Opens the database connection. Must be called after the constructor is called.
 * @param dbObj
 * @param populateFunc
 * @return {boolean}
 */
exports.open = function (dbObj, populateFunc) {
    var db = dbObj.db;
    var dbcollection = dbObj.dbCollectionName;
    db.open(function (err, db) {
        if (!err) {
            console.log("Connected to database");
            db.collection(dbcollection, {safe: true}, function (err, collection) {
                if (err) {
                    console.log("The collection doesn't exist.");
                    if (typeof populateFunc === 'function') {
                        populateFunc();
                    }
                    else {
                        return false;
                    }
                }
            });
        }
    });
    return true;
};
exports.insert = function (object, dbCollection, dbCollectionName){
    dbCollection(dbCollectionName, function(err, collection){
        collection.insert(object, {safe: true}, function(err, result){
            if(err){
                return false;
            }
            else{
                return true;
            }
        });
    });
};

exports.getAll = function(dbCollection, dbCollectionName){
    dbCollection(dbCollectionName, function(err, collection){
        collection.find().toArray(function(err, items){
            return items;
        })
    });
}