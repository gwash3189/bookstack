var express = require('express'),
    app = express();
app.configure(function(){
    app.use(express.logger('dev'));
    app.use(express.compress());
    app.use(express.bodyParser());
});

app.listen(8080, 'localhost');
console.log('Server running on http://localhost:8080');